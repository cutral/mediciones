---
layout: default
---

<article class="message">
  <div class="message-body has-text-centered">
    <b>Zomarist Met 50/1000, 2 x día (almuerzo/cena)</b>
  </div>
</article>

<div class="container is-size-4">

<table class="table is-striped is-narrow">
  <thead>
    <tr>
      <th class="has-text-centered"></th>
      <th class="has-text-centered">A</th>
      <th class="has-text-centered">DA</th>
      <th class="has-text-centered">DC</th>
    </tr>
  </thead>
  <tbody>
  {% for medicion in site.data.mediciones %}
    <tr>
      <td class="has-text-centered">{{ medicion.fecha }}</td>

      {% assign ayuna = medicion.a | abs %}
      <td class="has-text-centered {% if ayuna >= 120 %} has-text-warning {% endif %}">
        {% if ayuna != 0 %} {{ ayuna }} {% endif %}
      </td>

      {% assign da = medicion.da | abs %}
      <td class="has-text-centered {% if da >= 140 %} has-text-warning {% endif %}">
        {% if da != 0 %} {{ da }} {% endif %}
      </td>

      {% assign dc = medicion.dc | abs %}
      <td class="has-text-centered {% if dc >= 140 %} has-text-warning {% endif %}">
        {% if dc != 0 %} {{ dc }} {% endif %}
      </td>

    </tr>
  {% endfor %}
  </tbody>
</table>

</div>
